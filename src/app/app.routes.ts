import healthApi from "../routes/health/health.routes";
import onesignalApi from "../routes/onesignal/onesignal.routes";
import { endpointLogger } from "../submodules/shared-library/utils/logger";

function routes(app:any) {
    app.use('/health', healthApi);
    app.use('/onesignal', endpointLogger, onesignalApi);
}

export default routes;

